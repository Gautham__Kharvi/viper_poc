//
//  AppPresenter.swift
//  VIPER_POC
//
//  Created by gautham kharvi on 15/10/20.
//

import UIKit

class UserListPresenter: UserListPresenterProtocol {
    
    weak var view: UserListViewProtocol?
    var interactor: UserListInteractorProtocol?
    var router: UserListRouterProtocol?
    var users: [UserModel]?
    
    func fetchData() {
        DispatchQueue.global(qos: .utility).async {
            self.interactor?.fetchData()
        }
    }
    
    func getNumberOfRows() -> Int {
        return users?.count ?? 0
    }
    
    func getUser(row: Int) -> UserModel? {
        return users?[row]
    }
    
    func showDetails(user: UserModel, vc: UIViewController) {
        router?.goToDetail(user: user, vc: vc)
    }
    
    func getTitle() -> String {
        return "Users List"
    }
}
extension UserListPresenter: InteractorToPresenterProtocol {
    
    func dataFetchSuccess(users: [UserModel]) {
        self.users = users
        view?.dataFetchSuccess()
    }
    
    func dataFetchFailure(error: String) {
        view?.fetchFailure(error: error)
    }
}
