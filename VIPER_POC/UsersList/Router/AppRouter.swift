//
//  AppRouter.swift
//  VIPER_POC
//
//  Created by gautham kharvi on 15/10/20.
//

import UIKit

class UserListRouter: UserListRouterProtocol {
    
    static func createModule(userVC: UsersViewController) {
        let presenter: UserListPresenterProtocol & InteractorToPresenterProtocol = UserListPresenter()
        let interactor: UserListInteractorProtocol = UserListInteractor()
        let router: UserListRouterProtocol = UserListRouter()
        userVC.presenter = presenter
        presenter.view = userVC
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
    }
    
    func goToDetail(user: UserModel, vc: UIViewController) {
        let detailVC = vc.storyboard?.instantiateViewController(identifier: "DetailViewController") as! DetailViewController
        UserDetailRouter.createModule(userDetailVC: detailVC, user: user)
        vc.navigationController?.pushViewController(detailVC, animated: true)
    }
}
