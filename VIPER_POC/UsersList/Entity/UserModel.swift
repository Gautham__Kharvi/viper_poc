//
//  UserModel.swift
//  VIPER_POC
//
//  Created by gautham kharvi on 15/10/20.
//

import Foundation

class UserModel: Codable {
    
    var name: String!
    var username: String?
    var email: String?
    
}
