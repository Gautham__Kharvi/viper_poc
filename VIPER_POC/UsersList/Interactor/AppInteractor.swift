//
//  AppInteractor.swift
//  VIPER_POC
//
//  Created by gautham kharvi on 15/10/20.
//

import Foundation

class UserListInteractor: UserListInteractorProtocol {
    weak var presenter: InteractorToPresenterProtocol?
    
    func fetchData() {
        
        guard let url = URL(string: BaseAPI) else {return}
        
        URLSession.shared.dataTask(with: url) { (_data, _response, _error) in
            
            if let error = _error {
                self.presenter?.dataFetchFailure(error: error.localizedDescription)
                return
            }
            
            if let response = _response as? HTTPURLResponse, response.statusCode == 200 {
                do {
                    var users = try JSONDecoder().decode([UserModel].self, from: _data!)
                    users = users.sorted(by: {$0.name < $1.name})
                    self.presenter?.dataFetchSuccess(users: users)
                }catch {
                    self.presenter?.dataFetchFailure(error: error.localizedDescription)
                }
            }
        }.resume()
        
    }
    
    
    
    
}
