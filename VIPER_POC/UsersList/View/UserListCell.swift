//
//  UserListCell.swift
//  VIPER_POC
//
//  Created by gautham kharvi on 16/10/20.
//

import UIKit
import SnapKit

class UserListCell: UITableViewCell {
        
    lazy var nameLabel: UILabel = {
        var label = UILabel(frame: .zero)
        label.numberOfLines = 0
        label.activateDynamicFont(style: .body)
        return label
    }()
    
    lazy var emailIdLabel: UILabel = {
        var label = UILabel(frame: .zero)
        label.numberOfLines = 0
        label.activateDynamicFont(style: .body)
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.addSubview(nameLabel)
        self.addSubview(emailIdLabel)
        self.selectionStyle = .none
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateData(user: UserModel) {
        nameLabel.text = user.name
        emailIdLabel.text = user.email
    }
    
    func setConstraints() {
        nameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.safeAreaLayoutGuide).offset(5)
            make.leading.trailing.equalTo(self.safeAreaLayoutGuide).offset(10)
        }
        emailIdLabel.snp.makeConstraints { (make) in
            make.top.equalTo(nameLabel.snp.bottom)
            make.leading.trailing.equalTo(self.safeAreaLayoutGuide).offset(10)
            make.bottom.equalTo(self.safeAreaLayoutGuide).offset(5)
        }
    }

}
