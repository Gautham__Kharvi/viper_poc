//
//  UsersViewController.swift
//  VIPER_POC
//
//  Created by gautham kharvi on 15/10/20.
//

import UIKit

class UsersViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var presenter: UserListPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UserListCell.self, forCellReuseIdentifier: "UserListCell")
        UserListRouter.createModule(userVC: self)
        presenter?.fetchData()
        navigationItem.title = presenter?.getTitle()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView(frame: .zero)
    }
    
    func reloadData() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}

extension UsersViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.getNumberOfRows() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserListCell") as? UserListCell
        if let user = presenter?.getUser(row: indexPath.row) {
            cell?.updateData(user: user)
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let _presenter = presenter else {return}
        presenter?.showDetails(user: _presenter.users![indexPath.row], vc: self)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}
extension UsersViewController: UserListViewProtocol {
    func dataFetchSuccess() {
        self.reloadData()
    }
    
    func fetchFailure(error: String) {
        print(error)
    }
}
