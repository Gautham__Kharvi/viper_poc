//
//  AppProtocols.swift
//  VIPER_POC
//
//  Created by gautham kharvi on 15/10/20.
//

import UIKit

protocol UserListPresenterProtocol: AnyObject {
    //view -> presenter
    var view: UserListViewProtocol? {get set}
    var interactor: UserListInteractorProtocol? {get set}
    var router: UserListRouterProtocol? {get set}
    var users: [UserModel]? {get set}
    
    func getNumberOfRows() -> Int
    func getUser(row: Int) -> UserModel?
    func fetchData()
    func showDetails(user: UserModel, vc: UIViewController)
    func getTitle() -> String
}
protocol UserListViewProtocol: AnyObject {
    // presenter -> View
    func dataFetchSuccess()
    func fetchFailure(error: String)
}
protocol UserListInteractorProtocol: AnyObject {
    //presenter -> Interactor
    var presenter: InteractorToPresenterProtocol? { get set }
    func fetchData()
}
protocol InteractorToPresenterProtocol: AnyObject {
    //Interactor -> presenter
    func dataFetchSuccess(users: [UserModel])
    func dataFetchFailure(error: String)
}
protocol UserListRouterProtocol: AnyObject {
    // presenter -> Router
    static func createModule(userVC: UsersViewController)
    func goToDetail(user: UserModel, vc: UIViewController)
}
