//
//  UILabel+Extension.swift
//  VIPER_POC
//
//  Created by gautham kharvi on 16/10/20.
//

import UIKit

extension UILabel {
    func activateDynamicFont(style: UIFont.TextStyle) {
        self.font = UIFont.preferredFont(forTextStyle: style)
        self.adjustsFontForContentSizeCategory = true
    }
}
