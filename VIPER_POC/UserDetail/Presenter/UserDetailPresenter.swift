//
//  UserDetailPresenter.swift
//  VIPER_POC
//
//  Created by gautham kharvi on 16/10/20.
//

import UIKit

class UserDetailPresenter: UserDetailPresenterProtocol {
    var userRouter: UserDetailRouterProtocol?
    var view: UserDetailViewProtocol?
    var user: UserModel?
    var font: UIFont?
    
    func getTitle() -> String {
        return "User Detail"
    }
    
    func load() {
        view?.showUserData(user: user!)
        font = UIFont.systemFont(ofSize: 16)
        
    }
    
    func backButtonAction(vc: UIViewController) {
        
    }
    
    func updateFont(type: FontSize) {
        guard let _font = font else {return }
        let size = type == FontSize.Increment ? _font.pointSize + 1 : _font.pointSize - 1
        font = font?.withSize(size)
        view?.updateFont(font: font ?? UIFont().withSize(16))
    }
    
    func getFont() -> UIFont {
        return font ?? UIFont().withSize(16)
    }
}
