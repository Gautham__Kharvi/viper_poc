//
//  UserDetailProtocols.swift
//  VIPER_POC
//
//  Created by gautham kharvi on 16/10/20.
//

import UIKit

enum FontSize {
    case Increment, Decrement
}

protocol UserDetailPresenterProtocol {
    //View -> presenter
    var userRouter: UserDetailRouterProtocol? {get set}
    var view: UserDetailViewProtocol? {get set}
    var font: UIFont? {get set}
    func load()
    func backButtonAction(vc: UIViewController)
    func getTitle() -> String
    func updateFont(type: FontSize)
    func getFont() -> UIFont
}

protocol UserDetailViewProtocol {
    //Presenter -> View
    func showUserData(user: UserModel)
    func updateFont(font: UIFont)
}

protocol UserDetailRouterProtocol {
    func goBackToUsersList(vc: UIViewController)
}
