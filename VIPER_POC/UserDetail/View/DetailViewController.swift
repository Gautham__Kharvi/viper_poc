//
//  DetailViewController.swift
//  VIPER_POC
//
//  Created by gautham kharvi on 16/10/20.
//

import UIKit
import SnapKit

class DetailViewController: UIViewController {

    lazy var nameLabel: UILabel = {
        var label = UILabel(frame: .zero)
        label.numberOfLines = 0
        label.textAlignment = .center
        label.activateDynamicFont(style: .title1)
        view.addSubview(label)
        return label
    }()
    
    lazy var emailIdLabel: UILabel = {
        var label = UILabel(frame: .zero)
        label.numberOfLines = 0
        label.textAlignment = .center
        label.activateDynamicFont(style: .body)
        view.addSubview(label)
        return label
    }()
    
    var presenter: UserDetailPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupConstraints()
        presenter?.load()
        navigationItem.title = presenter?.getTitle()
    }
    
    func setupConstraints() {
        nameLabel.snp.makeConstraints { (make) in
            make.centerX.centerY.equalToSuperview()
            make.leading.trailing.equalTo(view.safeAreaLayoutGuide).inset(10)
        }
        emailIdLabel.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalTo(nameLabel.snp.bottom).offset(10)
            make.leading.trailing.equalTo(view.safeAreaLayoutGuide).inset(10)
        }
    }
}
extension DetailViewController: UserDetailViewProtocol {
    func showUserData(user: UserModel) {
        nameLabel.text = user.name
        emailIdLabel.text = user.email
    }
    
    func updateFont(font: UIFont) {
        nameLabel.font = font
        emailIdLabel.font = font
    }
}
