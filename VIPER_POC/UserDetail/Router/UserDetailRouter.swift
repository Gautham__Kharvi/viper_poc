//
//  UserDetailRouter.swift
//  VIPER_POC
//
//  Created by gautham kharvi on 16/10/20.
//

import UIKit

class UserDetailRouter: UserDetailRouterProtocol {
    
    static func createModule(userDetailVC: DetailViewController, user: UserModel) {
        let presenter = UserDetailPresenter()
        presenter.user = user
        presenter.userRouter = UserDetailRouter()
        presenter.view = userDetailVC
        userDetailVC.presenter = presenter
        
    }
    
    func goBackToUsersList(vc: UIViewController) {
        
    }
    
    
}
