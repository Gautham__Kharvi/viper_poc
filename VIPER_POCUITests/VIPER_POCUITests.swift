//
//  VIPER_POCUITests.swift
//  VIPER_POCUITests
//
//  Created by gautham kharvi on 15/10/20.
//

import XCTest

class VIPER_POCUITests: XCTestCase {

    
    var app: XCUIApplication!
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        app = XCUIApplication()
        
        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_Title() {
        app.launch()
        XCTAssert(app.staticTexts["Users List"].exists)
    }

    func testTableData() {
        app.launch()
        let tablesQuery = app.tables
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Lucio_Hettinger@annie.ca"]/*[[".cells.staticTexts[\"Lucio_Hettinger@annie.ca\"]",".staticTexts[\"Lucio_Hettinger@annie.ca\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        
        let usersListButton = app.navigationBars["User Detail"].buttons["Users List"]
        usersListButton.tap()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Rey.Padberg@karina.biz"]/*[[".cells.staticTexts[\"Rey.Padberg@karina.biz\"]",".staticTexts[\"Rey.Padberg@karina.biz\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        XCTAssert(app.staticTexts["User Detail"].exists)
        
        
        usersListButton.tap()
        app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .table).element.swipeUp()
        tablesQuery/*@START_MENU_TOKEN@*/.cells.containing(.staticText, identifier:"Glenna Reichert").element/*[[".cells.containing(.staticText, identifier:\"Chaim_McDermott@dana.io\").element",".cells.containing(.staticText, identifier:\"Glenna Reichert\").element"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.swipeDown()
        tablesQuery/*@START_MENU_TOKEN@*/.cells.containing(.staticText, identifier:"Patricia Lebsack").element/*[[".cells.containing(.staticText, identifier:\"Julianne.OConner@kory.org\").element",".cells.containing(.staticText, identifier:\"Patricia Lebsack\").element"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        usersListButton.tap()
        tablesQuery/*@START_MENU_TOKEN@*/.cells.containing(.staticText, identifier:"Kurtis Weissnat").element/*[[".cells.containing(.staticText, identifier:\"Telly.Hoeger@billy.biz\").element",".cells.containing(.staticText, identifier:\"Kurtis Weissnat\").element"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        usersListButton.tap()
    }
    
    func testUserDetail() {
        app.launch()
        let tablesQuery = app.tables
        tablesQuery.staticTexts["Lucio_Hettinger@annie.ca"].tap()
        XCTAssert(app.staticTexts["User Detail"].exists)
        XCTAssert(app.staticTexts["Lucio_Hettinger@annie.ca"].exists)
        XCTAssert(app.staticTexts["Chelsey Dietrich"].exists)
    }

}
